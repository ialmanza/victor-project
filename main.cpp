//
//  main.cpp
//  VictorFinalProject
//
//  Created by Isaac Almanza on 04/25/18.
//  Copyright © 2018 Isaac Almanza. All rights reserved.
//

#include <iostream>

using namespace std;


struct Cliente {
	char nombre[30];
	int cantidad[200];
	float precio[200];
	char estado;
	float saldo[200];
};


void morosidad(Cliente *clientes, char estado) {
	for (int b = 0;b<=10;b++){
		if (clientes[b].estado == estado && clientes[b].saldo[b] > 1000){
			cout << "El nombre del cliente: ";
			cin >> clientes[b].nombre;
			
			cout << "El saldo del cliente #"<< b+1 <<": es " << clientes[b].saldo[b];
			
		}
	}
}

void checarEstado(Cliente *clientes) {
	for (int b = 0;b<=10;b++){
		cout << "El saldo del cliente #"<< b+1 <<": es " << clientes[b].saldo[b];
		cout << "Su estado es: "<< clientes[b].estado;
	}
}
int main(int argc, const char * argv[]) {
	
	int option = 1;
	while (option >= 1 && option <=2){
		
		
		Cliente clientes[10];
		
		int i = 0;
		while (i <= 10){
			cout << "\n Introduzca el nombre del cliente: ";
			cin >> clientes[i].nombre;
			
			char s = 's';
			while (s == 's') {
				cout << "Introduzca la cantidad de unidades solicitadas: ";
				cin >> clientes[i].cantidad[i];
				
				cout << "Introduzca el precio de la unidad solicitada: ";
				cin >> clientes[i].precio[i];
				
				clientes[i].saldo[i] = clientes[i].cantidad[i] *  clientes[i].precio[i];
				
				cout << "Desea seguir introduciendo unidades S/N: ";
				cin >> s;
			}
			
			cout << "Introduzca el estado - M(MOROSO) - A(ATRASADO) - P(PAGADO): ";
			cin >> clientes[i].estado;
			
			i++;
		}
		
		
		cout << "\n Desea 1. 2.Ver saldo de cliente y estado, 3.Ver clientes con morosidad: ";
		cin >> option;
		
		switch (option) {
			case 1:
				checarEstado(clientes);
				break;
			case 2:
				char moroso =  'M';
				morosidad(clientes, moroso);
				break;
		}
		
	}
	

	
	
	return 0;
}